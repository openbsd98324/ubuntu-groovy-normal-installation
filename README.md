# ubuntu-groovy-normal-installation

## Content

The Ubuntu AMD64 Groovy Classic Installation, preinstalled to run from /dev/sda4.


![](desktop-ubuntu-groovy.png)



## Download 

Download the tarball ubuntu-groovy-normal-installation





## Installation 

cd /media/sda4 ; tar xvpfz ubuntu-groovy-normal-installation.tar.gz 

Preinstalled to sda4.






## Boot with Grub 

Add to grub:

```
menuentry 'Linux UBUNTU AMD64 on sda4 (gaming)' --class devuan --class gnu-linux --class gnu { 

        set root='hd0,msdos4'

        linux   /boot/vmlinuz-5.8.0-25-generic   root=/dev/sda4  rw 

        initrd  /boot/initrd.img-5.8.0-25-generic

}
```






